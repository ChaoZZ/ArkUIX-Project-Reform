# ArkUI-X 常见crash、白屏问题定位指南

	当前ArkUI-X应用开发者会因为一些原因出现应用crash、白屏问题，而从现有原生平台（Android、iOS）日志无法分析定位具体的原因，本篇指导以安卓为例，主要列举常见的crash、白屏问题，用来辅助应用开发者在部分crash和白屏场景下能够快速锁定问题并及时解决。

## Crash

##### Java找不到native方法

```shell
// 关键日志：
java.lang.UnsatisfiedLinkError: No implementation found for ...
```

```shell
// 举例：
java.lang.UnsatisfiedLinkError: No implementation found for void ohos.stage.ability.adapter.StageActivityDelegate.nativeAttachStageActivity(java.lang.String, ohos.stage.ability.adapter.StageActivity) (tried Java_ohos_stage_ability_adapter_StageActivityDelegate_nativeAttachStageActivity and Java_ohos_stage_ability_adapter_StageActivityDelegate_nativeAttachStageActivity__Ljava_lang_String_2Lohos_stage_ability_adapter_StageActivity_2)
at ohos.stage.ability.adapter.StageActivityDelegate.nativeAttachStageActivity(Native Method)
at ohos.stage.ability.adapter.StageActivityDelegate.attachStageActivity(StageActivityDelegate.java:47)
at ohos.stage.ability.adapter.StageActivity.onCreate(StageActivity.java:106)
at com.huawei.smarthome.arkui.activity.BaseArkActivity.onCreate(BaseArkActivity.java:34)
at com.huawei.smarthome.arkui.activity.DeviceCommonArkActivity.onCreate(DeviceCommonArkActivity.java:47)
```

**问题原因：** 运行时找不到对应的c++方法，这个异常是应用StageActivity构造前没有调用initApplication方法加载跨平台引擎库（libarkui_android.so）

**解决方案：** 应用拉起StageActivity之前，需要手动调用initApplication方法进行引擎库的初始化

##### C++运行异常

```shell
// 关键日志
在日志中搜"backtrace"可找到对应的crash堆栈
```

```shell
// 举例：
12-24 19:24:55.418 2023 2023 F libc : Fatal signal 11 (SIGSEGV), code 1 (SEGV_MAPERR), fault addr 0x0 in tid 2023 (i.phoneservice1), pid 2023 (i.phoneservice1)
12-24 19:24:55.516 2542 2542 F DEBUG : *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
12-24 19:24:55.516 2542 2542 F DEBUG : Build fingerprint: 'HUAWEI/HMA-AL00/HWHMA:10/HUAWEIHMA-AL00/10.1.0.163C00:user/release-keys'
12-24 19:24:55.516 2542 2542 F DEBUG : Revision: '0'
12-24 19:24:55.516 2542 2542 F DEBUG : ABI: 'arm64'
12-24 19:24:55.520 2542 2542 F DEBUG : SYSVMTYPE: Maple
12-24 19:24:55.520 2542 2542 F DEBUG : APPVMTYPE: Art
12-24 19:24:55.521 2542 2542 F DEBUG : Timestamp: 2024-12-24 19:24:55+0800
12-24 19:24:55.521 2542 2542 F DEBUG : pid: 2023, tid: 2023, name: i.phoneservice1 >>> com.huawei.phoneservice1 <<<
12-24 19:24:55.521 2542 2542 F DEBUG : uid: 10214
12-24 19:24:55.521 2542 2542 F DEBUG : signal 11 (SIGSEGV), code 1 (SEGV_MAPERR), fault addr 0x0
12-24 19:24:55.521 2542 2542 F DEBUG : Cause: null pointer dereference
12-24 19:24:55.521 2542 2542 F DEBUG : x0 0000000000000000 x1 0000000007800dd8 x2 0000007ff7a464a8 x3 0000000000000002
12-24 19:24:55.521 2542 2542 F DEBUG : x4 0000000000000156 x5 00000076a7aaab3f x6 73752f617461642f x7 6d6f632f302f7265
12-24 19:24:55.521 2542 2542 F DEBUG : x8 49a7f67d385ddd35 x9 49a7f67d385ddd35 x10 0000000000000419 x11 0000007698cc4fc0
12-24 19:24:55.521 2542 2542 F DEBUG : x12 000000770980d648 x13 736f5f6d726f6674 x14 0000002e500c3d20 x15 ffff000000000006
12-24 19:24:55.521 2542 2542 F DEBUG : x16 000000778e321a30 x17 000000778e315f80 x18 ffff000000000006 x19 0000007ff7a465dc
12-24 19:24:55.521 2542 2542 F DEBUG : x20 0000000000000000 x21 000000764dacd170 x22 000000778f477020 x23 00000076990d6add
12-24 19:24:55.521 2542 2542 F DEBUG : x24 000000769c30bc38 x25 0000007698217e44 x26 0000000000000004 x27 0000007ff7a468d0
12-24 19:24:55.521 2542 2542 F DEBUG : x28 0000007ff7a46940 x29 0000007ff7a465a0
12-24 19:24:55.521 2542 2542 F DEBUG : sp 0000007ff7a464f0 lr 000000769827fd00 pc 000000769827fd10
12-24 19:24:55.900 2542 2542 F DEBUG :
12-24 19:24:55.900 2542 2542 F DEBUG : backtrace:
12-24 19:24:55.900 2542 2542 F DEBUG : #00 pc 00000000027d5d10 /data/app/com.huawei.phoneservice1-qq35yfTQ1fj0xUBsWIi2EA==/lib/arm64/libarkui_android.so (OHOS::Ace::StringUtils::StringToDimensionWithUnit(std::__ndk1::basic_string<char, std::__ndk1::char_traits<char>, std::__ndk1::allocator<char>> const&, OHOS::Ace::DimensionUnit, float, bool)+72) (BuildId: 7b7a0ecfc892f8f5a29f2f3256422720)
12-24 19:24:55.900 2542 2542 F DEBUG : #01 pc 000000000276cf68 /data/app/com.huawei.phoneservice1-qq35yfTQ1fj0xUBsWIi2EA==/lib/arm64/libarkui_android.so (OHOS::Ace::Framework::JsiClass<OHOS::Ace::Framework::JSSwiperController>::JSConstructorInterceptor(panda::JsiRuntimeCallInfo*)+152) (BuildId: 7b7a0ecfc892f8f5a29f2f3256422720)
12-24 19:24:55.900 2542 2542 F DEBUG : #02 pc 000000000276def4 /data/app/com.huawei.phoneservice1-qq35yfTQ1fj0xUBsWIi2EA==/lib/arm64/libarkui_android.so (OHOS::Ace::Framework::JSSymbol::Create(OHOS::Ace::Framework::JsiCallbackInfo const&)+28) (BuildId: 7b7a0ecfc892f8f5a29f2f3256422720)
12-24 19:24:55.900 2542 2542 F DEBUG : #03 pc 000000000307e588 /data/app/com.huawei.phoneservice1-qq35yfTQ1fj0xUBsWIi2EA==/lib/arm64/libarkui_android.so (panda::SharedFloat32ArrayRef::New(panda::ecmascript::EcmaVM const*, panda::Local<panda::SendableArrayBufferRef>, int, int)+56) (BuildId: 7b7a0ecfc892f8f5a29f2f3256422720)
```

**问题原因：** 跨平台当前不支持symbol组件的功能，在内部的具体方法中发生了crash异常

**解决方案：** 出现类似的异常后，可以拿堆栈信息联系SDK分析

## 页面白屏

##### ArkCompiler异常报错

```shell
// 关键日志
ArkCompiler: [default] Throw error: Cannot read property ...
```

###### 应用libs目录未集成对应的动态库

**问题原因：** libs目录下缺少库libintl.so，有可能是IDE或者Ace Tools工具的问题

**解决方案：** 如果重复构建仍缺少，可以手动到ArkUI-X SDK目录下找到该.so完成拷贝，并把问题反馈给SDK

```shell
// 关键日志：
ArkCompiler: [default] GetNativeModuleValue:185 GetNativeModuleValueByIndex: currentModule /data/storage/el1/bundle/entry/ets/modules.abc, find requireModule
```

```shell
// 举例：
08-14 08:55:20.603  4354  4354 W ArkCompiler: [default] GetNativeModuleValue:185 GetNativeModuleValueByIndex: currentModule /data/storage/el1/bundle/entry/ets/modules.abc, find requireModule @ohos:intl failed
08-14 08:55:20.603  4354  4354 D ArkCompiler: [default] Throw error: Cannot read property NumberFormat of undefined
08-14 08:55:20.603  4354  4354 D ArkCompiler: [default]     at CategoryPage (entry/src/main/ets/pages/CategoryPage.ets:47:37)
08-14 08:55:20.603  4354  4354 D ArkCompiler:     at anonymous (entry/src/main/ets/pages/CategoryPage.ets:429:26)
08-14 08:55:20.603  4354  4354 E ArkCompiler: [default] Call:2170 occur exception need return
```

###### 动态化场景，被依赖的so库无法指定到沙箱目录加载，需要在apk预置

**问题原因：** libcrypto_openssl.so被libutil.so依赖，libutil.so可以动态下载，但被依赖so库只能预置

**解决方案：** 将libcrypto_openssl.so必须预置在apk中

```shell
// 举例：
09-12 19:20:28.128  6265  6265 D NAPI    : [native_module_manager.cpp(LoadNativeModule)] module 'util' does not in cache
09-12 19:20:28.128  6265  6265 D NAPI    : [native_module_manager.cpp(IsExistedPath)] pathKey is 'default'
09-12 19:20:28.128  6265  6265 D NAPI    : [native_module_manager.cpp(IsExistedPath)] pathKey is 'default'
09-12 19:20:28.128  6265  6265 D NAPI    : [module_load_checker.cpp(CheckModuleLoadable)] Not check moduleLoadable, moduleCheckerDelegate_ not set
09-12 19:20:28.128  6265  6265 D NAPI    : [native_module_manager.cpp(FindNativeModuleByDisk)] moduleName: util. get primary module path: /data/user/0/com.huawei.health/files/arkui-x/libs/arm64-v8a/libutil.so
09-12 19:20:28.128  6265  6265 D NAPI    : [native_module_manager.cpp(GetNativeModuleHandle)] moduleKey is 'util'
09-12 19:20:28.128  6265  6265 I NAPI    : [native_module_manager.cpp(LoadModuleLibrary)] path: /data/user/0/com.huawei.health/files/arkui-x/libs/arm64-v8a/libutil.so, pathKey: default, isAppModule: 0
09-12 19:20:28.131  6265  6265 W NAPI    : [native_module_manager.cpp(LoadModuleLibrary)] dlopen failed: dlopen failed: library "libcrypto_openssl.so" not found
09-12 19:20:28.131  6265  6265 D NAPI    : [native_module_manager.cpp(EmplaceModuleLib)] modulekey is 'util'
09-12 19:20:28.131  6265  6265 D NAPI    : [native_module_manager.cpp(FindNativeModuleByDisk)] try to load secondary module path: /data/user/0/com.huawei.health/files/arkui-x/libs/arm64-v8a/libutil.so
09-12 19:20:28.131  6265  6265 D NAPI    : [native_module_manager.cpp(GetNativeModuleHandle)] moduleKey is 'util'
09-12 19:20:28.131  6265  6265 I NAPI    : [native_module_manager.cpp(LoadModuleLibrary)] path: /data/user/0/com.huawei.health/files/arkui-x/libs/arm64-v8a/libutil.so, pathKey: default, isAppModule: 0
09-12 19:20:28.133  6265  6265 W NAPI    : [native_module_manager.cpp(LoadModuleLibrary)] dlopen failed: dlopen failed: library "libcrypto_openssl.so" not found
09-12 19:20:28.133  6265  6265 D NAPI    : [native_module_manager.cpp(EmplaceModuleLib)] modulekey is 'util'
09-12 19:20:28.133  6265  6265 D NAPI    : [native_module_manager.cpp(FindNativeModuleByDisk)] try to load abc module path: 
09-12 19:20:28.133  6265  6265 E NAPI    : [native_module_manager.cpp(GetFileBuffer)]  is not existed.
09-12 19:20:28.133  6265  6265 E NAPI    : [native_module_manager.cpp(FindNativeModuleByDisk)] all path load module 'util' failed
09-12 19:20:28.133  6265  6265 D NAPI    : [native_module_manager.cpp(LoadNativeModule)] load native module failed
09-12 19:20:28.133  6265  6265 W ArkCompiler: [default] GetNativeModuleValue:185 GetNativeModuleValueByIndex: currentModule /data/storage/el1/bundle/arkuix/ets/modules.abc, find requireModule @ohos:util failed
09-12 19:20:28.133  6265  6265 D ArkCompiler: [default] Throw error: Cannot read property LRUCache of undefined
09-12 19:20:28.133  6265  6265 D ArkCompiler: [default]     at static_initializer (features/arkuix/build/arkuix/cache/default/default@CompileArkTS/esmodule/release/oh_modules/.ohpm/@health+framework-base@1.6.3/oh_modules/@health/framework-base/src/main/ets/components/log/category/oh-logger.ts:7:7)
09-12 19:20:28.133  6265  6265 D ArkCompiler:     at func_main_0 (features/arkuix/build/arkuix/cache/default/default@CompileArkTS/esmodule/release/oh_modules/.ohpm/@health+framework-base@1.6.3/oh_modules/@health/framework-base/src/main/ets/components/log/category/oh-logger.ts:6:1)
```

###### 应用ets代码逻辑异常

**问题原因：** 在生命周期方法中，应用代码异常，通常会导致白屏

**解决方案：** 应用侧排查ets代码，自行修复

```shell
// 例如：在aboutToappear方法中使用了未赋值的对象test[0]
aboutToAppear(): void {
    let my=this.test[0].toString();
  }
```

```shell
// 日志
08-14 10:38:17.485 25500 25500 D ArkCompiler: [default] Throw error: Cannot read property toString of undefined
08-14 10:38:17.485 25500 25500 D ArkCompiler: [default]     at aboutToAppear (entry/src/main/ets/pages/CategoryPage.ets:138:31)
08-14 10:38:17.485 25500 25500 E ArkCompiler: [default] Call:2170 occur exception need return
08-14 10:38:17.485 25500 25500 E ArkCompiler: [default] Pending exception before ExecutePendingJob called, in line:3518, exception details as follows:
08-14 10:38:17.485 25500 25500 E ArkCompiler: [default] TypeError: Cannot read property toString of undefined
```

##### Web组件加载失败

**问题原因：** Android P以上手机，如果应用是多进程状态，需要为不同进程webView设置不同目录，否则web组件加载失败，也会导致白屏。

```shell
// 日志举例
06-07 10:55:36.858  1759  1759 E Ace     : [ace_resource_register.cpp(CreateResource)-(1)] AceResourceRegister CreateResource: has exception
06-07 10:55:36.859  1759  1759 W System.err: java.lang.RuntimeException: Using WebView from more than one process at once with the same data directory is not supported. https://crbug.com/558377 : Current process com.huawei.smarthome:device (pid 1759), lock owner  unknown
06-07 10:55:36.859  1759  1759 W System.err:     at org.chromium.android_webview.AwDataDirLock.b(HwWebview-12.0.4.307.4397:27)
06-07 10:55:36.860  1759  1759 W System.err:     at org.chromium.android_webview.AwBrowserProcess.k(HwWebview-12.0.4.307.4397:5)
```

**解决方案：** 在原生应用代码中为WebView设置不同目录

```shell
 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
    String processName = getProcessName(this);
    String packageName = this.getPackageName();
    if (!packageName.equals(processName)) {
    	WebView.setDataDirectorySuffix(processName);
    }
 }
```

##### assets目录缺少跨平台模块产物

**问题原因：** apk（非动态化加载）构建时，assets目录下没有拷贝对应的跨平台模块产物，需要check下图对应的一致性

**解决方案：** DevEco Studio或ACE Tools重新触发构建，如果拷贝仍缺失，联系SDK修复

​![](assets/61e6c856-0e49-4e94-82e9-cf505c52d1b6-20250114201648-244h64j.png)​

```shell
// 日志举例
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                I  [app_main.cpp(82)] module list size: 0
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                I  [app_main.cpp(72)] AppMain schedule launch application.
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                I  [stage_asset_provider.cpp(101)-(-1:-1:undefined)] Get module json buffer list
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                I  [app_main.cpp(82)] module list size: 0
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                E  [bundle_container.cpp(132)] bundleInfo_ is nullptr
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                I  [application.cpp(41)] Application::SetApplicationContext
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                E  [app_main.cpp(190)] applicationInfo or bundleInfo is nullptr.
2024-09-03 16:06:44.827  2901-2901  Ace                     com.huawei.hmos.world                I  [app_main.cpp(109)] Launch application success.
```

##### API实际不支持跨平台，标签误标

**问题原因：** 一些组件或api误标了跨平台标签，实际未适配无法使用

**解决方案：** 提供具体的API接口，以及ArkUI-X SDK版本号，联系SDK修复

场景一：

```shell
// 日志举例
ArkCompiler: [default] [GetNativeOrCjsExports:50] Load native module failed, so is xxx
ArkCompiler: [default] Throw error: the requested module 'xxx' does not provide an export name 'xxx' which imported by 'xxx'
```

场景二：

```shell
// 日志举例
ArkCompiler: [default] ReferenceError:  xxx is not defined
```

‍
